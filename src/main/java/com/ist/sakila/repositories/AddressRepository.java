package com.ist.sakila.repositories;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ist.sakila.models.Address;

public interface AddressRepository extends JpaRepository<Address, BigInteger>{
    
}
