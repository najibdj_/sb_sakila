package com.ist.sakila.models;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import com.ist.sakila.DTOs.AddressResponseDTO;

// import com.vividsolutions.jts.geom.Geometry;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "address")
public class Address {
    @Id
    @Column(name = "address_id", columnDefinition = "bigint")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger address_id;

    @Column(nullable = false, length = 50)
    private String address;
    
    @Column(length = 50)
    private String address2;

    @Column(nullable = false, length = 20)
    private String district;

    @ManyToOne()
    @JoinColumn(name = "city_id", referencedColumnName = "city_id", nullable = false)
    private City city;

    @Column(length = 10)
    private String postal_code;

    @Column(nullable = false, length = 20)
    private String phone;

    // @Column(nullable = false, columnDefinition = "GEOMETRY")
    // @Type(type = "org.hibernate.spatial.GeometryType")
    // private Geometry location;

    @UpdateTimestamp
    @Column(nullable = false)
    private LocalDateTime last_update;

    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Staff> staffs;
    
    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Store> stores;

    public AddressResponseDTO convertToResponse(){
        return AddressResponseDTO.builder()
            .address_id(this.address_id)
            .address(this.address)
            .address2(this.address2)
            .district(this.district)
            .postal_code(this.postal_code)
            .phone(this.phone)
            .build();
    }
}
