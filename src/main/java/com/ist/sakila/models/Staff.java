package com.ist.sakila.models;

import java.math.BigInteger;
import java.sql.Blob;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;

import com.ist.sakila.DTOs.StaffResponseDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "staff")
public class Staff {
    @Id
    @Column(name = "staff_id", columnDefinition = "bigint")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger staff_id;

    @Column(length = 45, nullable = false)
    private String first_name;

    @Column(length = 45, nullable = false)
    private String last_name;

    @ManyToOne
    @JoinColumn(name = "address_id", referencedColumnName = "address_id", nullable = false)
    private Address address;

    private Blob picture;

    @Column(length = 50)
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", referencedColumnName = "store_id", nullable = false)
    private Store store;

    @Column(length = 1, columnDefinition = "smallint")
    private Integer active;

    @Column(nullable = false, length = 16)
    private String username;
    
    @Column(length = 40)
    private String password;

    @UpdateTimestamp
    @Column(nullable = false)
    private LocalDateTime last_update;

    public StaffResponseDTO convertToResponse(){
        return StaffResponseDTO.builder()
            .staff_id(this.staff_id)
            .first_name(this.first_name)
            .last_name(this.last_name)
            .address(this.address.convertToResponse())
            .email(this.email)
            .active(this.active)
            .username(this.username)
            .build();
    }
}
