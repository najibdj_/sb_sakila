package com.ist.sakila.DTOs;

import java.math.BigInteger;
import java.sql.Blob;

import com.ist.sakila.models.Address;
import com.ist.sakila.models.Staff;
import com.ist.sakila.models.Store;
import com.ist.sakila.services.AddressService;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StaffRequestDTO {
    private BigInteger staff_id;

    private String first_name;

    private String last_name;
    
    private Blob picture;

    private Address address;

    private BigInteger address_id;

    private String email;

    private String username;

    private String password;

    private Store store;

    private AddressService addressService;

    public Staff convertToEntity(){
        return Staff.builder()
            .staff_id(this.staff_id)
            .first_name(this.first_name)
            .last_name(this.last_name)
            .address(this.address)
            .picture(this.picture)
            .email(this.email)
            .username(this.username)
            .password(this.password)
            .store(this.store)
            .build();
    }
}
