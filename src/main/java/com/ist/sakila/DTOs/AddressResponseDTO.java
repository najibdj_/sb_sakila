package com.ist.sakila.DTOs;

import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class AddressResponseDTO {
    private BigInteger address_id;

    private String address;

    private String address2;

    private String district;

    private String postal_code;

    private String phone;
}
