package com.ist.sakila.DTOs;

import java.math.BigInteger;

import com.ist.sakila.models.Address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressRequestDTO {
    private BigInteger address_id;

    private String address;
    
    private String address2;

    private String district;

    private String postal_code;

    private String phone;

    public Address convertToEntity(){
        return Address.builder()
            .address_id(this.address_id)
            .address(this.address)
            .address2(this.address2)
            .district(this.district)
            .postal_code(this.postal_code)
            .phone(this.phone)
            .build();
    }
}
