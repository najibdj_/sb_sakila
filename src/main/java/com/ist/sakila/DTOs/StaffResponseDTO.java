package com.ist.sakila.DTOs;


import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class StaffResponseDTO {
    private BigInteger staff_id;

    private String first_name;

    private String last_name;

    private AddressResponseDTO address;

    private String email;

    private Integer active;

    private String username;
}
