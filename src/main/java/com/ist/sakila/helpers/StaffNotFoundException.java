package com.ist.sakila.helpers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class StaffNotFoundException extends RuntimeException {
    public StaffNotFoundException(){
        super();
    }
}
