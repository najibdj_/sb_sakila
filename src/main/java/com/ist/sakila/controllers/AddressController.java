package com.ist.sakila.controllers;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.ist.sakila.DTOs.AddressResponseDTO;
import com.ist.sakila.models.Address;
import com.ist.sakila.services.AddressService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class AddressController {
    private AddressService addressService; 

    @GetMapping("/addresses")
    public ResponseEntity<List<AddressResponseDTO>> getAddresses(){
        List<Address> addressList = addressService.getAll();

        List<AddressResponseDTO> addressResponseDTOList = addressList.stream()
            .map(address -> address.convertToResponse())
            .collect(Collectors.toList());

        return ResponseEntity.ok(addressResponseDTOList);
    }

    @GetMapping("/addreess/{id}")
    public ResponseEntity<AddressResponseDTO> getAddress(@PathVariable("id") BigInteger id){
        Address address = addressService.getAddressById(id);

        AddressResponseDTO addressResponseDTO = address.convertToResponse();
        return ResponseEntity.ok(addressResponseDTO);
    }
}
