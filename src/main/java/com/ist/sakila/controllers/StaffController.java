package com.ist.sakila.controllers;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ist.sakila.models.Address;
import com.ist.sakila.models.Staff;
import com.ist.sakila.models.Store;
import com.ist.sakila.repositories.AddressRepository;
import com.ist.sakila.repositories.StaffRepository;
import com.ist.sakila.repositories.StoreRepository;
import com.ist.sakila.services.StaffService;
import com.ist.sakila.DTOs.StaffResponseDTO;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class StaffController {
    private StaffService staffService;

    private StaffRepository staffRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private StoreRepository storeRepository;

    /***
     * Get all data in at staff table
     * @return all data of all column in staff table
     */
    @GetMapping("/staffs")
    public ResponseEntity<List<StaffResponseDTO>> getStaffs(){
        List<Staff> staffList = this.staffService.getAll();

        List<StaffResponseDTO> staffResponseDTOList = staffList.stream()
            .map(staff -> staff.convertToResponse())
            .collect(Collectors.toList());
            
        return ResponseEntity.ok(staffResponseDTOList);
    }

    /***
     * Get one data at staff table 
     * @param id id of data that is going to be shown
     * @return data of staff which id is @param id
     */
    @GetMapping("/staff/{id}")
    public ResponseEntity<StaffResponseDTO> getStaff(@PathVariable("id") BigInteger id){
        Staff staff = staffService.getStaffById(id);

        StaffResponseDTO staffResponseDTO = staff.convertToResponse();
        return ResponseEntity.ok(staffResponseDTO);
    }

    /***
     * Create new data for table staff 
     * @param staffRequestDTO 
     * @return
     */
    @PostMapping("/staff")
    public ResponseEntity<Staff> createStaff(@RequestBody Staff staff){
        try{
            Address address = addressRepository.getReferenceById(staff.getAddress().getAddress_id());
            Store store = storeRepository.getReferenceById(staff.getStore().getStore_id());
            staff.setAddress(address);
            staff.setStore(store);
            staffRepository.save(staff);
            return ResponseEntity.status(HttpStatus.CREATED).body(staff);
        } catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(staff);
        }
    }
}
