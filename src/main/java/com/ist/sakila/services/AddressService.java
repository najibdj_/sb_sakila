package com.ist.sakila.services;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ist.sakila.helpers.AddressNotFoundException;
import com.ist.sakila.models.Address;
import com.ist.sakila.repositories.AddressRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class AddressService {
    private final AddressRepository addressRepository;

    public List<Address> getAll(){
        return this.addressRepository.findAll();
    }

    public Address getAddressById(BigInteger id){
        Optional<Address> optionalAddress =  this.addressRepository.findById(id);
        if(optionalAddress.isEmpty()){
            throw new AddressNotFoundException();
        }
        return optionalAddress.get();
    }
}
