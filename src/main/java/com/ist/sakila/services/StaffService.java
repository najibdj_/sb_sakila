package com.ist.sakila.services;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.ist.sakila.helpers.StaffNotFoundException;
import com.ist.sakila.models.Staff;
import com.ist.sakila.repositories.StaffRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class StaffService {
    private final StaffRepository staffRepository;

    public List<Staff> getAll(){
        return this.staffRepository.findAll();
    }

    public Staff getStaffById(BigInteger id){
        Optional<Staff> optionalStaff = this.staffRepository.findById(id);
        if (optionalStaff.isEmpty()){
            throw new StaffNotFoundException();
        }
        return optionalStaff.get();
    }

    public Staff createStaff(Staff staff){
        return this.staffRepository.save(staff);
    }
}
